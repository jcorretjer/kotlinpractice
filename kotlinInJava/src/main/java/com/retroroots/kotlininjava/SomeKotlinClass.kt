package com.retroroots.kotlininjava


class SomeKotlinClass
{
    fun printSomething()
    {
        println("from Kotlin")
    }

    companion object
    {
        lateinit var instance: SomeKotlinClass
    }
}