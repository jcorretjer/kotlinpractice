package com.retroroots.kotlinvariables

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity()
{
    //Must have lateInit if it's global in a class
     private lateinit var vr3: String

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        //No need to use new
        val someClass = SomeClass("value")

        val someClass2 = SomClass2()

        someClass2.param3 = "val3"

        println("param3 value " + someClass2.param3)

        SomeJavaClass.getInstance().callKotlin()
    }
}