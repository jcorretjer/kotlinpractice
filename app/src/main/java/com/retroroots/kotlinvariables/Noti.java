package com.retroroots.kotlinvariables;

public class Noti
{
    private String name;

    public static class Builder
    {
        private String name;

        public Builder SetName(String name)
        {
            this.name = name;

            return this;
        }

        public Noti Build()
        {
            return new Noti();
        }
    }
}

class Test
{
    public Test()
    {
        Noti noti = new Noti.Builder()
                .SetName("")
                .Build();
    }
}
