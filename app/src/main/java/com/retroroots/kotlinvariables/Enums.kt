package com.retroroots.kotlinvariables

import java.util.*

class StringDef
{
    enum class Statuses
    {
        VALID,
        INVALID;

        fun GetFormattedName() = name.toLowerCase(Locale.getDefault()).capitalize(Locale.getDefault())
    }

}

fun printSomething(param: String, status: StringDef.Statuses)
{
    when(status)
    {
        StringDef.Statuses.VALID -> println(param)

        else -> println("Status not valid")
    }
}

fun printStatus(status: StringDef.Statuses)
{
    println(status.name)

    println(status.GetFormattedName())
}

fun main()
{
    printSomething("Status works", StringDef.Statuses.VALID)

    printSomething("Status works", StringDef.Statuses.INVALID)

    printStatus(StringDef.Statuses.VALID)

    printStatus(StringDef.Statuses.INVALID)
}