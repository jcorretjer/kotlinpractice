package com.retroroots.kotlinvariables

fun someMethod(): String
{
    return "val"
}

fun someMethod2(): String =
    "value"

fun someMethod3() =
    "value"

fun someMethd4(param: String) =
    "$param dude"

//By giving it a default value in the signature it's not required that we pass any of these parameters
fun someMethd5(param: String = "param", param2: String = "param2") =
    "$param $param2 dude"

fun someMethd6(param: String, param2: String) =
    "$param $param2 dude"

fun main()
{
    println(someMethd4("fart"))

    println(someMethd5( "flaming","fart"))

    //Passing a named argument I can mix the order
    println(someMethd5( param2 = "flaming", param = "fart"))

    println(someMethd5( "fart"))

    println(someMethd5())
}