package com.retroroots.kotlinvariables

fun main()
{
    //Kotlin switch statement

    var vr4: String? = null

    when(vr4)
    {
        null -> println("Is null")

        else -> println("Not null")
    }

    //endregion

    //region Assign condition expression to variable

    vr4 = if(vr4 == null) "Is null" else vr4

    //Elvis operator is like a binary version of a ternary operator. true ?: false.
    // So it returns what's on the left of the operator if not null else return right side
    vr4 = vr4
          ?: "Is null"

    println(vr4)

    vr4 = if(vr4 != null) null else vr4

    println(vr4)

    vr4 = when(vr4)
    {
        null -> "Is null"

        else -> "Not null"
    }

    println(vr4)

    //endregion
}