package com.retroroots.kotlinvariables

// InterfaceImpl access modifier must be set to open to extend classes
class Inheritance: InterfaceImpl()
{
    //Kind of like extending classes, class variables must also be set to open to be overriden

    override val someVal: String
        get() = "super.someVal overriden"
}