package com.retroroots.kotlinvariables;

public class SomeJavaClass
{
    public static SomeJavaClass getInstance()
    {
        return new SomeJavaClass();
    }

    public void callKotlin()
    {
        SomeKotlinClass.instance.printSomething();
    }
}
