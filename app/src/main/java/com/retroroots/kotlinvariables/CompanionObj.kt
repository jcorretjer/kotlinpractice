package com.retroroots.kotlinvariables

class CompanionObj private constructor(param: String)
{
    /*
        It's kind of like a static variable wrapper. In kotlin, there is no static keyword.
        El companion es como una clase regular que puedes extender o inherit.
     */
    companion object Creator : SomeInterface
    {
        fun create(param: String) = CompanionObj(param)

        override fun meth()
        {
            println("meth() override from companion obj")
        }
    }
}

fun main()
{
    /*
    Como el constructor esta privado no se puede accesar, pero el companion tiene acceso a cosas privadas y funciona como un gate para usar cosas privadas.
    Asi 'CompanionObj.Companion.create("value")' funciona tambien. En Kotlin, el compaion se puede omitir y usar asi CompanionObj.create.
    O si el compaion tiene un nombre pues seria asi CompanionObj.Creator.create para ser mas directo.
     */
    val companionObj = CompanionObj.create("value")

    CompanionObj.meth()
}