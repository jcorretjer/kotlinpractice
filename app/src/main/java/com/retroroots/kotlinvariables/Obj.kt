package com.retroroots.kotlinvariables

object ObjFactory
{
    fun create() = Obj()
}

class Obj ()
{
}

fun main()
{
    val companionObj = ObjFactory.create()
}