package com.retroroots.kotlinvariables

fun main()
{
    //region Array

    val array = arrayOf("1", "2")

    //foreach
    for (ar in array)
        println(ar)

    array.forEach {
        println(it)
    }

    //Changed default "it" name to "ar"
    array.forEach { ar ->
        println(ar)
    }

    array.forEachIndexed { index, ar ->  println("item $ar is at index $index")  }

    //endregion

    //region List

    //Immutable
    val list = listOf("-1", "-2")

    //Mutable
    val list2 = mutableListOf("-1", "-2")

    list2.add("--1")

    //foreach
    for (ar in list)
        println(ar)

    list.forEach {
        println(it)
    }

    //Changed default "it" name to "ar"
    list.forEach { ar ->
        println(ar)
    }

    list.forEachIndexed { index, ar ->  println("list item $ar is at index $index")  }

    //endregion

    //region Map

    //Immutable
    val map = mapOf(1 to "-2", 2 to "-3")

    //Mutable
    val map2 = mutableMapOf(1 to "-2", 2 to "-3")

    map2.put(3, "--3")

    map.forEach { (k, v) -> println("key $k for value $v") }

    //endregion

    someMethod(list, list2, map, map2)

    //* = spread and allows to pass that array instead of explicitly passing each parameter.
    someOtherMethod(*array)
}

fun someMethod(items: List<String>, items2: MutableList<String>, items3: Map<Int, String>, items4: MutableMap<Int, String>)
{
    items.forEach { println(it) }

    items2.add("another")

    items2.forEach { println(it) }

    items3.forEach { (k, v) -> println("key $k for value $v") }

    items4.put(10, "another val")

    items4.forEach { (k, v) -> println("key $k for value $v") }
}

fun someOtherMethod(vararg items:String)
{
    items.forEach { println(it) }
}