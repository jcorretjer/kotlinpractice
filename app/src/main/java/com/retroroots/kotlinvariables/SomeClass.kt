package com.retroroots.kotlinvariables

class SomeClass constructor(_param:String)
{
    //Another way to initialize property
    //val param:String = _param

    val param: String

    //Runs class
    init
    {
        param = _param

        println("class 2 init. Args $param")
    }
}