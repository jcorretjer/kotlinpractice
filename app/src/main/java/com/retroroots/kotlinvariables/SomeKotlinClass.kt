package com.retroroots.kotlinvariables

class SomeKotlinClass
{
    fun printSomething()
    {
        println("from Kotlin")
    }

    companion object
    {
        lateinit var instance: SomeKotlinClass

        init
        {
            instance = SomeKotlinClass()
        }
    }
}