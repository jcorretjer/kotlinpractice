package com.retroroots.kotlinvariables

/**
 * Data classes are like pojo classes in kotlin. They are made to work as model classes only
 *
 * By setting it to val means that this value, once it's passed as an argument it will not be changed.
 */
data class DataClasses(val param:String, var param2: String)

fun main()
{
    val dataClass = DataClasses("value 1", "value 2")

    //Throws error because it's val
    //dataClass.param = "new value 3"

    dataClass.param2 = "new value 3"

    println(dataClass)

    //Copy creates a copy of this instance and if desired can pass new arguments.
    println(dataClass.copy(param = "value 4"))
}
