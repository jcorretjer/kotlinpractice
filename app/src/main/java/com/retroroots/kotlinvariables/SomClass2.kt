package com.retroroots.kotlinvariables

class SomClass2(val param:String = "value", var param2: String? = null)
{
    var param3: String = ""
    set(value) //This is how you override a setter. The are also auto generated, but can override
    {
        field = value

        println("param3 set to $value")
    }
    get()
    {
        print("Returning $field for param3 value. ")

        return field
    }

    //All inits run before all secondary constructor
    init
    {
        println("class 2 init. Args $param and $param2")
    }

    //Secondary constructor. This runs only if it's using default value
    /*constructor(): this("value")
    {
        println("2nd constructor")
    }*/
}