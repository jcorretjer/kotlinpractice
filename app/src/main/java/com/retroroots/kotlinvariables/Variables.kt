package com.retroroots.kotlinvariables

fun main()
{
    //region Var. They are regular variable keywords

    //This is of type String none null
    var mutableName = ""

    var vr: String

    var vr2: String = ""

    //This is a nullable String
    var vr4: String? = "empty"

    println(vr4)

    vr4 = null

    println(vr4)

    //endregion

    //region Vals. They are variable constants

    val inmuttableName = ""

    val name5: String

    name5 = ""

    //endregion
}