package com.retroroots.kotlinvariables

interface SomeInterface
{
    fun meth()

    //This becomes like an abstract method
    fun meth2()
    {
        println("meth2")
    }
}

interface SomeInterface2
{
    fun met()

    //This becomes like an abstract method
    fun met2()
    {
        println("meth2")
    }
}

open class InterfaceImpl : SomeInterface, SomeInterface2
{
    open val someVal = "a value"

    override fun meth()
    {
        println("meth")
    }

    override fun met()
    {
        println("met")
    }
}

class InterfaceImpl2 : SomeInterface
{
    override fun meth()
    {
        println("meth")
    }
}

fun main()
{
    InterfaceImpl().meth()

    InterfaceImpl().meth2()

    println(InterfaceImpl().someVal)

    println(Inheritance().someVal)

    //Instead of creating a new instance I can create an extension like this
    val someInterface = object : SomeInterface
    {
        override fun meth()
        {
            println("Object expression")
        }
    }

    someInterface.meth()
}